#!/usr/bin/env python
#
# This script terminates with 0 if either:
# 1. no fetch/pull operation have yet been performed on the repo
# 2. no more that 300 seconds passed since last fetch/pull
#
import os
import sys
import time
import pathlib


def main():
    filename = ".git/FETCH_HEAD"

    fetch_head_file = pathlib.Path(filename)
    if not fetch_head_file.exists():
        sys.exit(0)  # new repository

    now = int(time.time())
    head = int(os.popen("stat -c %Y .git/FETCH_HEAD").read())

    elapsed = now - head
    print("{} seconds elapsed from your latest pull.".format(elapsed))

    ret_cod = 0 if elapsed <= 300 else 1
    sys.exit(ret_cod)


if __name__ == "__main__":
    main()
